import os
import datetime

from multiprocessing import Process

from pymongo import MongoClient
from humbledb import Mongo
from bson.objectid import ObjectId
from sqlalchemy import create_engine
from contracts import contract

from cli_passthrough import passthrough

from sampleprojectdpewebapp.settings import DB_URL, CLUSTER_DIR


def test_pgdb_conn():
    raw_engine = create_engine(DB_URL)
    raw_conn = raw_engine.connect()
    raw_conn.execute(
        """CREATE TABLE IF NOT EXISTS films (title text,
        director text,
        year text)"""
    )
    result = raw_conn.execute(
        """select column_name,
        data_type from information_schema.columns where table_name = 'films'"""
    )
    rows = result.fetchall()
    raw_conn.close()

    s = ""
    for row in rows:
        s = s + str(row) + "\n"
    return s


def test_mgdb_conn():
    client = MongoClient("localhost", 27017)
    db = client["test-database"]
    collection = db["test-collection"]
    articles = collection.articles
    # articles.remove({"author": "Mike"})
    if articles.find_one({"author": "Mike"}) is None:
        post = {"author": "Mike", "text": "My first blog post!", "tags": ["mongodb", "python", "pymongo"], "date": datetime.datetime.utcnow()}
        articles.insert_one(post)

    for article in articles.find():
        print(article)
    return "result.inserted_id"
