import cherrypy

from sampleprojectdpewebapp.flask_app import app

from sampleprojectdpewebapp.settings import server_port, socket_host

cherrypy.tree.graft(app, "/")
cherrypy.config.update({"server.socket_host": socket_host, "server.socket_port": server_port, "engine.autoreload.on": False})

if __name__ == "__main__":
    cherrypy.engine.start()
