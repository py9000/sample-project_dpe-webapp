import os
import tempfile

from sampleprojectdpewebapp.utils import get_env_variable

PROJECT_NAME = "sampleprojectdpewebapp"

# std config vars
POSTGRES_URL = get_env_variable("POSTGRES_URL")
POSTGRES_USER = get_env_variable("POSTGRES_USER")
POSTGRES_PW = get_env_variable("POSTGRES_PW")
POSTGRES_DB = get_env_variable("POSTGRES_DB")

# we should refactor this into f-strings
DB_URL = "postgresql+psycopg2://{user}:{pw}@{url}/{db}".format(user=POSTGRES_USER, pw=POSTGRES_PW, url=POSTGRES_URL, db=POSTGRES_DB)

MONGO_DB = PROJECT_NAME

VERSION = "0.2"

UPLOAD_FOLDER = "uploads"

ALLOWED_EXTENSIONS = set(["txt", "pdf", "png", "jpg", "jpeg", "gif", "zip"])

TEMPDIR = "/dev/shm"

BASEDIR = os.path.abspath(os.path.dirname(__file__))

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

TEMPLATE_DIR = os.path.join(ROOT_DIR, "templates")

STATIC_DIR = os.path.join(ROOT_DIR, "static")

TEXTTABLE_STYLE = ["-", "|", "+", "-"]

server_port = 5000

socket_host = "0.0.0.0"

# special config objects
CONFIG_DIC = {"POSTGRES_URL": POSTGRES_URL, "POSTGRES_USER": POSTGRES_USER, "POSTGRES_PW": POSTGRES_PW, "POSTGRES_DB": POSTGRES_DB}

tempfile.tempdir = TEMPDIR
