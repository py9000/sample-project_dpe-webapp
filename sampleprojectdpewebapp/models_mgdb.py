from humbledb import Document

from sampleprojectdpewebapp.settings import MONGO_DB


class test_mgdb(Document):
    config_database = MONGO_DB
    config_collection = "test data"
    description = "d"
    value = "v"
