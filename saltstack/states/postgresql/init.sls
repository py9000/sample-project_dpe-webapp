apt_update_for_postgresql:
  cmd.run:
    - name: apt -y update

install_postgresql:
  cmd.run:
    - name: sudo apt -y install postgresql postgresql-contrib

start_postgresql:
  cmd.run:
    - name: sudo service postgresql start

set_postgresql_password:
  cmd.run:
    - name: sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'PASSWORD';"
